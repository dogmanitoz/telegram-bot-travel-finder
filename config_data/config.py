import os
from dotenv import load_dotenv, find_dotenv
from telebot.storage import StateMemoryStorage

if not find_dotenv():
    exit('Переменные окружения не загружены т.к отсутствует файл .env')
else:
    load_dotenv()

TOKEN = os.getenv('TOKEN')
X_RAPIDAPI_KEY = os.getenv('X_RAPIDAPI_KEY')
storage = StateMemoryStorage()
