import telebot
from handlers import bot_handlers
from loguru import logger
from config_data import config
from keyboards.inline.bot_inline_calendar import show_calendar
from telebot import types
from typing import Dict


bot = telebot.TeleBot(config.TOKEN, state_storage=config.storage)

hotel_messages: Dict = {'low': ['Ищем отели с минимальными ценами.', 'PRICE'],
                        'high': ['Ищем самые дорогие отели в этом городе.', 'PRICE_HIGHEST_FIRST'],
                        'best':
                            ['Ищем отели по соотношению цены и расстояния до центра города', 'DISTANCE_FROM_LANDMARK'],
                        'error':
                            ['Ошибка ввода. Лучше начните сначала. Будут выведены отели с низкими ценами.', 'PRICE']}


@bot.message_handler(commands=['start', 'help', 'lowprice', 'highprice', 'bestdeal'])
def get_commands(message: telebot.types.Message) -> None:
    """
    Функция, которая обрабатывает команды пользователя
    :param message: Полученное в чате сообщение
    """
    bot_handlers.bot_command(bot, message)


@bot.message_handler(content_types=['text'])
def get_text_messages(message: telebot.types.Message) -> None:
    """
    Функция, которая обрабатывает текстовые сообщения
    :param message: Сообщение из чата
    """
    if message.text.capitalize() == 'Привет':
        bot.send_message(message.chat.id,
                         'Привет, {0.first_name}'.format(message.from_user, bot.get_me()),
                         parse_mode='html')
    elif message.text.capitalize() == 'Как дела?':
        bot.send_message(message.chat.id, 'Отлично, у тебя как?', parse_mode='html')
    else:
        bot_handlers.bot_command(bot, message)


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call: telebot.types.CallbackQuery) -> None:
    """
    Обработчик событий inline-клавиатуры
    :param call: сообщение от inline-клавиатуры
    """

    logger.info(f'call {call.from_user.id}')
    logger.info(call.data)
    bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id)
    if call.data.startswith('calendar'):
        name, action, year, month, day = call.data.split(':')
        if action == 'DAY':
            bot_handlers.set_date(call)
        else:
            if action == 'NEXT-MONTH':
                month = str(int(month) + 1)
            elif action == 'PREVIOUS-MONTH':
                month = str(int(month) - 1)
            show_calendar(bot, call.message, int(month), name=name)
    else:
        capt_list = call.data.split(sep='.')
        if capt_list[1] == 'loc':
            bot_handlers.get_lang(capt_list[0], call, bot)
        elif capt_list[1] == 'city':
            bot_handlers.get_start(capt_list[0], call.message, bot)
        elif capt_list[1] == 'hot':
            bot_handlers.chosen_hotel(capt_list[0], call, bot)
        elif capt_list[1].endswith('pic'):
            bot_handlers.get_picts(capt_list[0], call.message, bot, capt_list[1])
        elif capt_list[1] == 'his':
            bot_handlers.show_history(call.message)


def service_message(bot, message: types.Message, sm_num: int) -> None:
    """
    Функция выводит пользователю сервисные сообщения про командам /help и /start
    :param bot: чат бот
    :param message: Полученное в чате сообщение
    :param sm_num: ключ в словаре service_message
    """
    service_messages: Dict = {0: 'Cледующие команды помогут Вам найти лучший для Вас отель:\n'
                                 '\'/lowprice\' - самые дешевые отели в городе\n'
                                 '\'/highprice\' - самые дорогие отели в городе\n'
                                 '\'/bestdeal\' - лучшее предложение по цене и расстоянию до центра\n'
                                 '\'/history\' - Просмотр результатов последнего запроса',
                              1: f'{bot.get_me().first_name} предназначен для поиска отелей в различных городах.\n'
                                 'Для продолжения нажмите /help или используйте Menu'}

    bot.send_message(message.from_user.id, service_messages[sm_num])


def check_mark(message: types.Message) -> bool:
    """
    Функция, которая проверяет сообщение на команду от пользователя
    :param message: сообщение от пользователя в чате
    :return: Возвращает True, если текст сообщения начинается с /
    """
    if message.text.startswith('/'):
        return True
