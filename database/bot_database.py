import peewee

from classes.db_class import Session, History
from loguru import logger
from typing import List, Any


@logger.catch
def create_tables(class_name: str) -> None:
    """
    Создает таблицу в БД
    :param class_name: название класса
    """
    logger.info(f'Попытка создания таблицы {class_name}')
    if not eval(class_name).table_exists():
        eval(class_name).create_table()
        logger.info('Таблица создана')
    else:
        logger.info('Таблица существует')


@logger.catch
def update_record(field: str, field_value: str, chat_id: int):
    """
    Обновляет значение поля записи в таблице
    :param field: название поля, которое надо изменить
    :param field_value: значение изменяемого поля
    :param chat_id: id чата, в котором происходит взаимодействие с ботом
    """
    query = eval(f"""Session.update({field}='{field_value}').where(
        (Session.chat_id == {chat_id}) &
        (Session.t_stamp == Session.select(peewee.fn.MAX(Session.t_stamp)).where(
        Session.chat_id == {chat_id}).scalar()))""")
    logger.info('Сохраняю изменения в базу')
    query.execute()


@logger.catch
def select_some(chat_id: int, class_name: str, *args: Any) -> List[Any]:
    """
    Производит выборку из таблицы базы данных
    :param chat_id: id чата, в котором происходит взаимодействие с ботом
    :param class_name: название класса, который используется внутри функции
    :param args: название полей, по которым нужно выполнить выборку из базы данных
    :return: результат запроса в виде списка
    """
    this_record = eval(class_name).select().dicts().where((
            (eval(class_name).chat_id == chat_id) &
            (eval(class_name).t_stamp == eval(class_name).select(peewee.fn.MAX(eval(class_name).t_stamp)).where(
                eval(class_name).chat_id == chat_id).scalar()))).get()
    result = [this_record.get(field) for field in args]
    return result


def select_history(chat_id: int, time_stamp: float, *args: Any) -> List[Any]:
    """
    Возвращает результат последнего запроса пользователя
    :param chat_id: id чата, в котором происходит взаимодействие с ботом
    :param time_stamp: таймстамп начала работы цепочки взаимодействия с пользователем
    :param args: название полей, по которым нужно выполнить выборку из базы данных
    :return: Результат запроса в виде списка
    """
    logger.info(type(time_stamp))
    logger.info(time_stamp)
    this_record = Session.select().dicts().where(Session.t_stamp == time_stamp).get()
    logger.info(this_record)
    result = [this_record.get(field) for field in args]
    return result


@logger.catch
def create_record(chat_id: int, state: str, time_stamp: float) -> None:
    """
    Добавляет в таблицу Sessions новую запись
    :param chat_id: id чата
    :param state: тип цепочки (low, high, best)
    :param time_stamp: таймстамп, после которой начинается цепочка
    """
    this_session = Session(chat_id=chat_id, state=state, lang='', city='', location='', hot_num='',
                           persons='', check_in='', check_out='', currency='', start_price='',
                           stop_price='', t_stamp=time_stamp, distance='')
    try:
        this_session.save()
        logger.info(f'Начало сессии с chat_id = {chat_id}, получена команда {state}')
    except Exception as err:
        logger.error(f'Произошла ошибка при добавлении записи сессии с chat_id = {chat_id}.')
        logger.error(err)


@logger.catch
def create_history(chat_id: int, hotels: str, tm_stamp: float) -> None:
    """
    Добавляет запись в БД историю запросов пользователя
    :param chat_id: id чата
    :param hotels: json-строка из списка отелей
    :param tm_stamp: время начала цепочки
    """
    this_session = History(chat_id=chat_id, hotels=hotels, t_stamp=tm_stamp)
    try:
        this_session.save()
        logger.info(f'Сохранение результатов запроса chat_id = {chat_id}')
    except Exception:
        logger.error(f'Произошла ошибка при сохранении результатов запроса с chat_id = {chat_id}.')
