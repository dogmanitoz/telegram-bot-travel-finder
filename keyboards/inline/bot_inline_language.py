from telebot import types
from keyboards.inline.botkeyboard_class import BotKeyboard


def get_locale(message: types.Message, bot) -> None:
    """
    Создает inline-клавиатуру для запроса языка у пользователя
    :param message: Полученное в чате сообщение
    :param bot: чат-бот
    """
    this_keyboard = BotKeyboard([('Русский', 'ru_RU.loc'), ('English', 'en_US.loc')], 2)
    keyboard = this_keyboard.create_keys()
    question = 'На каком языке будем искать?'
    bot.send_message(message.from_user.id, text=question, reply_markup=keyboard)
