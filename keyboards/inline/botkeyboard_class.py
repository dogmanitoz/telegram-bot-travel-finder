from typing import List, Tuple
from telebot import types


class BotKeyboard:

    """
    Класс, реализующий inline keyboard
    """

    def __init__(self, keys: List[Tuple], rows: int):
        """
        первичная инициализация класса
        :param keys: список, содержащий названия и возвращаемые значения кнопок
        :param rows: количество столбцов в клавиатуре
        """
        self.keys: List = keys
        self.rows: int = rows
        self.key_list: List = []

    @property
    def keys(self) -> List:
        """
        Возвращает список кнопок
        :return: список кнопок
        """
        return self._keys

    @keys.setter
    def keys(self, keys: List) -> None:
        """
        Инициализирует переменную, содержащую список кнопок
        :param keys: список кнопок
        """
        self._keys = keys[:]

    @property
    def rows(self) -> int:
        """
        Возвращает количество стобцов клавиатуры
        :return: количество столбцов клавиатуры
        """
        return self._rows

    @rows.setter
    def rows(self, rows: int) -> None:
        """
        Инициализирует переменную, содержащую количество столбцов
        :param rows: количество столбцов клавиатуры
        """
        self._rows = rows

    def create_keys(self) -> types.InlineKeyboardMarkup:
        """
        Создает и возвращает inline-клавиатуру
        :return:собственно, клавиатура
        """
        bot_keyboard = types.InlineKeyboardMarkup(row_width=self.rows)
        for one_key in self.keys:
            self.key_list.append(types.InlineKeyboardButton(one_key[0], callback_data=one_key[1]))
        for all_keys in self.key_list:
            bot_keyboard.add(all_keys)
        return bot_keyboard
