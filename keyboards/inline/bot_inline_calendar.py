import datetime
from telebot import types
from telebot_calendar import Calendar, RUSSIAN_LANGUAGE, CallbackData

calendar = Calendar(language=RUSSIAN_LANGUAGE)
calendar_1_callback = CallbackData("calendar_1", "action", "year", "month", "day")
now = datetime.datetime.now()
count = 0


def show_calendar(bot,
                  message: types.Message,
                  month: int,
                  question: str = 'Выберите дату заезда в отель',
                  name: str = calendar_1_callback.prefix) -> None:
    """
    Создание inline-клавиатуры календаря
    :param bot: чат-бот
    :param message: Полученное в чате сообщение
    :param month: номер месяца
    :param question: передаваемый пользователю вопрос
    :param name: имя передаваемого пользователю календаря
    """
    global count
    if month == 13:
        month = 1
        count += 1
    year = now.year + count
    if name == 'calendar_2':
        question = 'Выберите дату выезда'
    bot.send_message(
        message.chat.id,
        question,
        reply_markup=calendar.create_calendar(
            name=name,
            month=month,
            year=year
        ),
    )
