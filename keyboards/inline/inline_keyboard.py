from typing import List
from utils.bot_commands import bot
from main import logger
from keyboards.inline.botkeyboard_class import BotKeyboard


def show_hotels(chat_id: int, hotels: List, question: str = 'Отели найдены. Выберите подходящий:') -> None:
    """
    Выводит inline-клавиатуру со списком отелей, попавших в критерии, указанные пользователем.
    :param chat_id: id чата
    :param hotels: Список, содержащий списки/кортежи, содержащие id и описание отелей, подходящих критериям пользователя
    :param question: вопрос пользователю
    """
    logger.info('Вызов функции show_hotels')
    logger.info(hotels)
    this_keyboard = BotKeyboard(hotels, 1)
    keyboard = this_keyboard.create_keys()
    bot.send_message(chat_id, text=question, reply_markup=keyboard)
