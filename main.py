from handlers import bot_handlers
import atexit
from loguru import logger
from database.bot_database import create_tables
from utils import bot_commands


@atexit.register
def goodbye() -> None:
    """
    Выводит сообщение при выходе
    """
    logger.info('Завершение')


# RUN
if __name__ == '__main__':
    logger.add('logs/travel_finder_bot.log', rotation="10 MB", encoding='utf-8')
    logger.info('Bot is starting')
    create_tables('Session')
    create_tables('History')
    try:
        bot_commands.bot.polling(none_stop=True, interval=0)
    except Exception as e:
        logger.opt(exception=True).error(f'Unexpected error: {e}')
