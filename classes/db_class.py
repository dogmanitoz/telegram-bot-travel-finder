import peewee


class MainModel(peewee.Model):
    """
    Основной класс Peewee
    определяет базу данных
    """

    class Meta:
        database = peewee.SqliteDatabase('database/bot_sessions.db')


class Session(MainModel):
    """
    Класс, определяющий таблицу запросов пользователя newsessions и методы работы с ней
    """
    chat_id = peewee.CharField()
    state = peewee.CharField()
    lang = peewee.CharField()
    city = peewee.CharField()
    location = peewee.CharField()
    hot_num = peewee.CharField()
    persons = peewee.CharField()
    check_in = peewee.CharField()
    check_out = peewee.CharField()
    currency = peewee.CharField()
    start_price = peewee.CharField()
    stop_price = peewee.CharField()
    t_stamp = peewee.FloatField()
    distance = peewee.CharField()

    class Meta:
        db_table = 'newsessions'


class History(MainModel):
    """
    Класс, определяющий таблицу истории запросов пользователя и методы работы с ней.
    """
    chat_id = peewee.CharField()
    hotels = peewee.TextField()
    t_stamp = peewee.FloatField()

    class Meta:
        db_table = 'newhistory'
