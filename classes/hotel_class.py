from typing import List, Dict


class ThisHotel:
    """
    Класс, предоставляющий информацию по выбранному отелю
    """

    def __init__(self, all_data: Dict):
        self.data_dict: Dict = all_data

    @property
    def data_dict(self) -> Dict:
        return self._data_dict

    @data_dict.setter
    def data_dict(self, all_data: Dict) -> None:
        """
        Инициализирует переменную, содержащую список всех данных отеля
        :param all_data: все данные об отеле в виде словаря
        """
        self._data_dict: Dict = all_data

    def show_name(self) -> str:
        """
        Возвращает название отеля
        :return: название отеля
        """
        return self.data_dict.get('data').get('body').get('propertyDescription').get('name')

    def show_coordinates(self) -> List:
        """
        Возвращает координаты отеля
        :return: список, содержащий координаты отеля
        """
        latitude = self.data_dict.get('data').get('body').get('pdpHeader') \
            .get('hotelLocation').get('coordinates').get('latitude')
        longitude = self.data_dict.get('data').get('body').get('pdpHeader') \
            .get('hotelLocation').get('coordinates').get('longitude')
        return [latitude, longitude]

    def show_overview(self) -> Dict:
        """
        Возвращает обзор отеля
        :return: словарь, содержащий обзор отеля
        """
        this_overview = self.data_dict.get('data').get('body').get('overview').get('overviewSections')[0].get('content')
        return this_overview

    def show_around(self) -> Dict:
        """
        Возвращает окружение отеля
        :return: словарь, содержащий описание того, что рядом с отелем
        """
        this_around = self.data_dict.get('data').get('body').get('overview').get('overviewSections')[1].get('content')
        return this_around

    def show_address(self) -> str:
        """
        Возвращает адрес отеля
        :return: адрес отеля
        """
        this_address = self.data_dict.get('data').get('body').get('propertyDescription').get('address').get(
            'fullAddress')
        return this_address

    def show_price(self) -> str:
        """
        Возвращает стоимость суток проживания в отеле
        :return: стоимость
        """
        this_price = str(self.data_dict.get('data').get('body').get('propertyDescription').get('featuredPrice')
                         .get('currentPrice').get('plain'))
        return this_price

    def get_all_info(self) -> str:
        """
        Собирает и возвращает строку, содержащую все данные об отеле
        :return: суммарно все данные об отеле в одной строке, разделенной символами перевода строки.
        """
        coords = self.show_coordinates()
        info_string = str(self.show_name() + '\n' + f'Координаты отеля: {coords[0]}, {coords[1]}\n\n' + 'Описание:\n' +
                          '\n'.join(self.show_overview()) + '\n' + '\n' + 'Что находится рядом с отелем:\n' +
                          '\n'.join(self.show_around()) + '\n' + 'Цена за одну ночь:\n' + self.show_price() +
                          '\n' + 'Адрес отеля:\n' + self.show_address())
        return info_string
