from typing import List, Dict, Tuple
import requests
from loguru import logger
from classes.hotel_class import ThisHotel


class ApiSearch:
    """
    Класс работы с rapidapi.com
    """

    def __init__(self, my_api: str, *args, **kwargs):
        """
        Инициализация переменных класса.
        :param токен подключения к rapidapi:
        :param args: urls and headers участвующие в запросе
        :param kwargs:
        """
        self.this_api = my_api
        self.this_query = args
        self._headers = {'x-rapidapi-key': self.this_api, 'x-rapidapi-host': "hotels4.p.rapidapi.com"}
        self._city_url = "https://hotels4.p.rapidapi.com/locations/search"
        self._hotels_url = "https://hotels4.p.rapidapi.com/properties/list"
        self._one_hotel_url = "https://hotels4.p.rapidapi.com/properties/get-details"
        self._hotel_pics_url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"

    @property
    def this_api(self) -> str:
        return self._this_api

    @this_api.setter
    def this_api(self, my_api: str):
        self._this_api: str = my_api

    @property
    def this_query(self) -> Tuple[str]:
        return self._this_query

    @this_query.setter
    def this_query(self, args: Tuple):
        self._this_query: Tuple = args

    def get_response(self, one_url: str, query: Dict) -> Dict:
        """
        Получает, сериализует и возвращает ответ от API
        В случае ошибки при получении ответа, возвращает пустой словарь
        :param one_url: url, по которому производится запрос
        :param query: словарь, содержащий переменные, участвующие в запросе
        :return: Dict
        """
        response = dict()
        try:
            logger.info(f'Посылаю запрос {query} на url {one_url}')
            response = requests.request("GET", one_url, headers=self._headers, params=query)
            logger.info(f'Получен ответ {response}')
        except Exception:
            logger.info('Произошла ошибка при обращении к API сайта')
        return response.json()

    def get_city(self) -> List[Tuple]:
        """
        Получает список id городов, имя которых совпадает с введенным пользователем
        В случае ошибки возвращает пустой список
        :return: список кортежей, содержащих имя города с географической привязкой и его id
        """
        try:
            querystring = {'query': self.this_query[0], 'locale': self.this_query[1]}
            found_city = self.get_response(self._city_url, querystring)
            cities = [(elem.get('caption'), elem.get('destinationId'))
                      for elem in found_city.get('suggestions', [])[0].get('entities')
                      if elem.get('type') == 'CITY' and self.this_query[0].lower() in elem.get('name').lower()]
        except IndexError:
            cities = []
            logger.warning('Получен неправильный ответ от сайта при запросе города.')
        return cities

    @logger.catch
    def get_hotels(self) -> List[Tuple]:
        """
        Получает список отелей, подходящих под критерии, введенные пользователем.
        В случае ошибки возвращает пустой список
        :return: список кортежей, содержащих информацию об отелях
        """
        hotels_list = []

        def hotel_description(hotel):
            return (f"{hotel.get('name')}\n{'⭐️' * int(hotel.get('starRating', 0))}\n "
                    f"{hotel.get('address').get('streetAddress')}. \n"
                    f"{str(hotel.get('ratePlan').get('price').get('exactCurrent'))} руб.\n"
                    f"{hotel.get('landmarks')[0].get('distance').split(sep=' ')[0]} \n до центра",
                    str(hotel.get('id')) + '.hot')
        try:
            querystring = {"adults1": self.this_query[0], "pageNumber": self.this_query[1],
                           "destinationId": self.this_query[2], "pageSize": self.this_query[3],
                           "checkOut": self.this_query[4], "checkIn": self.this_query[5],
                           "sortOrder": self.this_query[6],
                           "locale": self.this_query[7], "currency": self.this_query[8]}
            if self.this_query[6] != 'DISTANCE_FROM_LANDMARK':
                logger.info(f'Команда {self.this_query[6]} в классе поиска отелей')
                found_hotels = self.get_response(self._hotels_url, querystring)
                hotels_list.extend([hotel_description(one_hotel)
                                    for one_hotel in found_hotels['data']['body']['searchResults']['results']])
            else:
                logger.info(f'Команда {self.this_query[6]} в классе поиска отелей, раздел bestdeal')
                if self.this_query[7] == 'en_US':
                    distance = round(float(self.this_query[10]) * 0.62, 2)
                else:
                    distance = float(self.this_query[10])
                querystring["priceMax"] = self.this_query[9]
                querystring["priceMin"] = '100'
                while len(hotels_list) < int(self.this_query[3]):
                    found_hotels: Dict = self.get_response(self._hotels_url, querystring)
                    next_page: str = found_hotels.get('data').get('body').get('searchResults').get(
                        'pagination').get("nextPageNumber")
                    if not next_page or int(next_page) <= int(querystring['pageNumber']):
                        break
                    logger.info(f'Номер следующей страницы: {next_page}')
                    querystring['pageNumber'] = next_page
                    hotels_list.extend([hotel_description(one_hotel)
                                        for one_hotel in found_hotels['data']['body']['searchResults']['results']
                                        if float(one_hotel.get('landmarks')[0].get(
                                            'distance').split(sep=' ')[0].replace(',', '.')) <= distance])
        except IndexError as err:
            logger.warning('Получен неправильный ответ от сайта при запросе отелей.')
            logger.warning(err)
        return hotels_list

    def get_one_hotel(self) -> str:
        """
        Получает от API и возвращает информацию об одном отеле. Использует для этого
        экземпляр класса ThisHotel
        :return: строка, содержащая полную информацию об отеле
        """
        try:
            querystring = {"id": self._this_query[0], "checkIn": self._this_query[1],
                           "checkOut": self._this_query[2], "adults1": self._this_query[3],
                           "currency": self._this_query[4], "locale": self._this_query[5]}
            this_hotel = self.get_response(self._one_hotel_url, querystring)
            if this_hotel.get('result') != 'OK':
                hotel_info = 'Произошла ошибка при обращении к сайту.'
            else:
                our_hotel = ThisHotel(this_hotel)
                hotel_info = our_hotel.get_all_info()
        except IndexError:
            hotel_info = 'Возникла непредвиденная ошибка, попробуйте снова.'
            logger.warning('Получен неправильный ответ от сайта при запросе конкретного отеля.')
            logger.warning(f'ID отеля: {self._this_query[0]}')
        return hotel_info

    def get_hotel_pics(self) -> List[str]:
        """
        Возвращает список url изображений отеля
        :return: список url изображений
        """
        pics: List = []
        try:
            querystring = {"id": self._this_query[0]}
            pictures = self.get_response(self._hotel_pics_url, querystring)
            one_hotel = pictures.get('hotelImages')
            if one_hotel:
                for one_pic in one_hotel:
                    pics.append(one_pic.get("baseUrl"))
            return pics
        except IndexError:
            logger.warning(f'Произошла ошибка при получении изображений отеля с id {self._this_query[0]}')
            return pics
