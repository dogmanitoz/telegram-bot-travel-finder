import time
import datetime
from database import bot_database
import json
from telebot import types
from typing import List, Tuple
from config_data import config
from utils.bot_commands import bot, service_message, check_mark, hotel_messages
from main import logger
from classes.apisearch_class import ApiSearch
from keyboards.inline.botkeyboard_class import BotKeyboard
from keyboards.inline import bot_inline_calendar, bot_inline_language, inline_keyboard


def bot_command(bot, message: types.Message) -> None:
    """
    Опрос пользователя, создание записи сессии в БД
    :param bot: чат-бот
    :param message: сообщение от пользователя
    """
    time_stamp = time.time()
    chat_id = message.chat.id
    logger.info(f'Начало цепочки: {message.text}, chat.id: {message.chat.id}')
    if message.text == '/help':
        service_message(bot, message, 0)
        state = 'help'
    elif message.text == '/start':
        service_message(bot, message, 1)
        state = 'start'
    elif message.text == '/lowprice':
        state = 'low'
        bot_inline_language.get_locale(message, bot)
    elif message.text == '/highprice':
        state = 'high'
        bot_inline_language.get_locale(message, bot)
    elif message.text == '/bestdeal':
        state = 'best'
        bot_inline_language.get_locale(message, bot)
    elif message.text == '/history':
        state = 'hist'
        show_history(message)
    else:
        bot.send_message(message.chat.id, 'К сожалению, эта команда мне неизвестна.\nПопробуйте еще раз.\n'
                                          'Для помощи, введите /help')
        state = 'error'
    if message.text in ('/lowprice', '/highprice', '/bestdeal'):
        bot_database.create_record(chat_id, state, time_stamp)


def get_lang(one_locale: str, in_message: types.CallbackQuery, bot) -> None:
    """
    Принимает выбранное значение из inline-клавиатуры, созданной в get_locale
    Производит запись полученного значения в базу данных
    :param one_locale: выбранный пользователем язык
    :param in_message: вызов из inline-клавиатуры
    :param bot: чат-бот
    """
    bot_database.update_record('lang', one_locale, in_message.message.chat.id)
    msg = bot.send_message(in_message.from_user.id, 'В каком городе ищем отели? ')
    bot.register_next_step_handler(msg, get_city, bot)


def get_city(message: types.Message, bot) -> None:
    """
    Вызывает find_city для получения списка городов. Если получен список более чем из одного элемента,
    выводит inline-клавиатуру для вывода вариантов пользователю. Если возвращен список из одного элемента,
    вызывает get_start, передает в нее id выбранного города.
    Если возвращен пустой список, выводит сообщение об ошибке, возвращает пользователя к выбору города.
    :param message: полученное сообщение
    :param bot: чат-бот
    """
    if check_mark(message):
        bot_command(bot, message)
    else:
        city: str = message.text
        bot_database.update_record('city', city, message.chat.id)
        cities: List = find_city(city, message)
        if len(cities) == 0:
            msg = bot.send_message(message.chat.id, 'Ничего не найдено\nПопробуйте изменить название:')
            bot.register_next_step_handler(msg, get_city, bot)
        elif len(cities) > 1:
            places: List[Tuple] = [(city + ',' + ''.join(one_city[0].split(sep=',')[1:]), one_city[1] + '.city')
                                   for one_city in cities]
            this_keyboard = BotKeyboard(places, 1)
            keyboard: types.InlineKeyboardMarkup = this_keyboard.create_keys()
            question: str = 'Найдено несколько городов. Выберите подходящий:'
            bot.send_message(message.from_user.id, text=question, reply_markup=keyboard)
        else:
            get_start(cities[0][1], message, bot)


def find_city(city: str, message: types.Message) -> List[Tuple]:
    """
    Создает объект класса ApiSearch, вызывает его метод get_city для получения списка id городов по названию,
    введенному пользователем. Возвращает этот список в get_city
    :param city: Название города, введенное пользователем.
    :param message: полученное сообщение
    :return: список кортежей, содержащих название с географической привязкой и id городов,
    название которых совпадает с введенным пользователем
    """
    lang: str = bot_database.select_some(message.chat.id, 'Session',  'lang')[0]
    searched_cities = ApiSearch(config.X_RAPIDAPI_KEY, city, lang)
    cities: List = searched_cities.get_city()
    return cities


def get_start(location: str, message, bot) -> None:
    """
    Производит запись полученного из get_city значения в базу данных.
    Отсылает пользователю сообщение о количестве отелей для поиска.
    Назначает обработчиком следующего шага hotel_numbers
    :param location: id выбранного города
    :param message: полученное сообщение
    :param bot: чат-бот
    """
    bot_database.update_record('location', location, message.chat.id)
    state = bot_database.select_some(message.chat.id, 'Session',  'state')[0]
    bot.send_message(message.chat.id, hotel_messages[state][0])
    bot.send_message(message.chat.id, 'Введите количество отелей для поиска.\n'
                                      'Не более 20 отелей в списке.')
    bot.register_next_step_handler(message, hotel_numbers, bot, location)


def hotel_numbers(message, bot, location: str) -> None:
    """
    Производит запись полученного от пользователя значения в базу данных
    В зависимости от выбранного сценария либо опрашивает пользователя о количестве проживающих,
    либо запрашивает максимальное расстояние до центра города. Назначает следующий обработчик сообщения
    :param message: Полученное в чате сообщение
    :param bot: чат-бот
    :param location: id выбранного города
    """
    if check_mark(message):
        bot_command(bot, message)
    else:
        hot_num = message.text
        if not hot_num.isdigit():
            bot.send_message(message.from_user.id, 'Неправильный ввод, попробуйте еще раз')
            bot.register_next_step_handler(message, hotel_numbers, bot, location)
        elif 0 >= int(hot_num) or int(hot_num) > 25:
            bot.send_message(message.from_user.id, 'Ошибка! Вы ввели неправильное количество отелей. Попробуйте снова.')
            bot.register_next_step_handler(message, hotel_numbers, bot, location)
        else:
            bot_database.update_record('hot_num', hot_num, message.chat.id)
            state = bot_database.select_some(message.chat.id, 'Session', 'state')[0]
            logger.info(f'После выбора отелей проверка критерия поиска: {state}')
            if state == 'best':
                logger.info('Запрос расстояния')
                msg = bot.send_message(message.from_user.id, 'Какое должно быть максимальное расстояние '
                                                             'от отеля до центра города (км)?')
                bot.register_next_step_handler(msg, get_landmarks, bot)
            else:
                msg = bot.send_message(message.from_user.id, 'Сколько человек планирует проживать в отеле?')
                bot.register_next_step_handler(msg, get_person, bot)


@logger.catch()
def get_landmarks(message: types.Message, bot):
    """
    Участвует только /bestdeal
    Сохраняет значение расстояния до центра города в базу. Задает вопрос пользователю о максимальной стоимости
    проживания. Назначает следующий обработчик
    :param message: Полученное в чате сообщение
    :param bot: чат-бот
    """
    if check_mark(message):
        bot_command(bot, message)
    else:
        distance = message.text
        if not distance.isdigit():
            msg = bot.send_message(message.from_user.id, 'Неправильный ввод. '
                                                         'Какое должно быть максимальное расстояние '
                                                         'от отеля до центра города (км)?')
            bot.register_next_step_handler(msg, get_landmarks, bot)
        else:
            bot_database.update_record('distance', distance, message.chat.id)
            msg = bot.send_message(message.from_user.id, 'Какова должна быть максимальная стоимость суток проживания '
                                                         '(RUB)?')
            bot.register_next_step_handler(msg, get_max_price, bot)


def get_max_price(message: types.Message, bot) -> None:
    """
    Участвует только /bestdeal
    Сохраняет значение цены проживания в базу. Задает вопрос пользователю о количестве проживающих.
    Назначает следующий обработчик
    :param message: Полученное в чате сообщение
    :param bot:чат-бот
    """
    if check_mark(message):
        bot_command(bot, message)
    else:
        max_price = message.text
        if not max_price.isdigit():
            msg = bot.send_message(message.from_user.id, 'Неправильный ввод. '
                                                         'Какова должна быть максимальная стоимость суток проживания'
                                                         ' (RUB)?')
            bot.register_next_step_handler(msg, get_max_price, bot)
        else:
            bot_database.update_record('stop_price', max_price, message.chat.id)
            msg = bot.send_message(message.from_user.id, 'Сколько человек планирует проживать в отеле?')
            bot.register_next_step_handler(msg, get_person, bot)


def get_person(message: types.Message, bot) -> None:
    """
    Сохраняет количество проживающих в базу. Вызывет создание inline-клавиатуры-календаря для получения дат
    заезда и выезда из отеля
    :param message: Полученное в чате сообщение
    :param bot: чат-бот
    """
    now = datetime.datetime.now()
    if check_mark(message):
        bot_command(bot, message)
    else:
        persons = message.text
        bot_database.update_record('persons', persons, message.chat.id)
        bot_inline_calendar.show_calendar(bot, message, month=now.month, question='Выберите дату заезда')


def get_check_out(message: types.Message, bot, currency: str = 'RUB') -> None:
    """
    Получает список отелей по заданным критериям. Вызывает show_hotels для вывода списка пользователю.
    :param message: types.Message
    :param bot: чат-бот
    :param currency: используемая валюта
    """
    if check_mark(message):
        bot_command(bot, message)
    else:
        bot.send_message(message.chat.id, 'Ведется поиск, пожалуйста, подождите...⏱')
        persons, location, hot_num, check_out, check_in, sorting_m, lang, max_price, distance, tm_stamp = \
            bot_database.select_some(message.chat.id, 'Session', 'persons', 'location', 'hot_num',
                                     'check_out', 'check_in', 'state', 'lang', 'stop_price', 'distance', 't_stamp')
        sorting_method = hotel_messages[sorting_m][1]
        page_number = "1"
        if sorting_m != 'best':
            searched_hotels = ApiSearch(config.X_RAPIDAPI_KEY, persons, page_number, location, hot_num, check_out,
                                        check_in, sorting_method, lang, currency)
        else:
            searched_hotels = ApiSearch(config.X_RAPIDAPI_KEY, persons, page_number, location, hot_num, check_out,
                                        check_in, sorting_method, lang, currency, max_price, distance)
        hotels_list = searched_hotels.get_hotels()
        if hotels_list:
            hotels_history = json.dumps(hotels_list, ensure_ascii=False)
            logger.info('Получен список отелей:')
            logger.info(hotels_history)
            bot_database.create_history(message.chat.id, hotels_history, tm_stamp)
            inline_keyboard.show_hotels(message.chat.id, hotels_list)
        else:
            logger.warning('Список отелей не получен', message.chat.id)
            bot.send_message(message.chat.id, 'К сожалению, ни одного отеля не найдено')


def chosen_hotel(hotel_id: str, call: types.CallbackQuery, bot, currency: str = 'RUB') -> None:
    """
    Выводит пользователю информацию по выбранному отелю. Предлагает на выбор вывод изображений отеля,
    либо просмотр результатов последнего запроса.
    :param hotel_id: id выбранного пользователем отеля
    :param call: вызов из inline -клавиатуры
    :param bot: чат-бот
    :param currency: используемая валюта
    """
    persons, check_out, check_in, lang = bot_database.select_some(
        call.message.chat.id, 'Session', 'persons', 'check_out', 'check_in', 'lang')
    logger.info(f'переменные для конкретного отеля: {call.message.chat.id}, {persons}, {check_out}, {check_in}, {lang}')
    one_hotel = ApiSearch(config.X_RAPIDAPI_KEY, hotel_id, check_in, check_out, persons, currency, lang)
    hotel_info = one_hotel.get_one_hotel()
    hotels = bot_database.select_some(call.message.chat.id, 'History', 'hotels')[0]
    hotels_list = json.loads(hotels)
    for one_hotel in hotels_list:
        if one_hotel[1].split(sep='.')[0] == hotel_id:
            distance = one_hotel[0].split(sep='\n')[-2]
            if lang == 'en_US':
                distance = round(float(distance) * 1.609, 2)
            hotel_info += f'\nРасстояние до центра города: {distance} км.'
    bot.send_message(call.message.chat.id, hotel_info)
    bot.send_message(call.message.chat.id, f'https://ru.hotels.com/ho{hotel_id}')
    next_step: List = [('Изображения отеля', hotel_id + '.h_pic'),
                       ('Показать последний запрос', str(call.message.chat.id) + '.his')]
    this_keyboard = BotKeyboard(next_step, 1)
    keyboard = this_keyboard.create_keys()
    bot.send_message(call.message.chat.id, text='Выберите действие: ', reply_markup=keyboard)


def set_date(call: types.CallbackQuery) -> None:
    """
    Получает даты заезда и выезда, записывает их в базу данных
    :param call: вызов из inline-клавиатуры
    """
    name, action, year, month, day = call.data.split(':')
    if len(day) == 1:
        day = '0' + day
    if len(month) == 1:
        month = '0' + month
    now = datetime.datetime.now()
    proper = False
    stage = 'none'
    this_date = f'{now.year}-{now.month}-{now.day}'
    if name == 'calendar_1':
        stage = 'check_in'
        if datetime.date(int(year), int(month), int(day)) >= datetime.date.today():
            this_date = f'{year}-{month}-{day}'
            proper = True
            logger.info('определена дата заезда')
    else:
        chk_in = bot_database.select_some(call.message.chat.id, 'Session', 'check_in')[0]
        logger.info(chk_in)
        if chk_in:
            c_year, c_month, c_day = map(int, chk_in.split(sep='-'))
            if datetime.date(int(c_year), int(c_month), int(c_day)) < datetime.date(int(year), int(month), int(day)):
                this_date = f'{year}-{month}-{day}'
                stage = 'check_out'
                proper = True
                logger.info('определена дата выезда')
    if proper:
        bot_database.update_record(stage, this_date, call.message.chat.id)
        if stage == 'check_in':
            bot_inline_calendar.show_calendar(bot, call.message, month=now.month, question='Выберите дату выезда', name='calendar_2')
            logger.info('Определяем дату выезда')
        else:
            get_check_out(call.message, bot)
    else:
        if name == 'calendar_1':
            action_now = 'въезда'
            bot.send_message(call.message.chat.id, 'Дата въезда указана ранее текущей даты.')
        else:
            action_now = 'выезда'
            bot.send_message(call.message.chat.id, 'Дата выезда указана ранее даты въезда.')
        bot.send_message(call.message.chat.id, 'Неправильный ввод, пожалуйста, повторите')
        bot_inline_calendar.show_calendar(bot, call.message, month=now.month,
                                          question=f'Выберите дату {action_now}', name=name)


def get_picts(hot_id: str, message: types.Message, bot, p_type: str) -> None:
    """
    Запрашивает у пользователя количество выводимых изображений отеля.
    :param hot_id: id выбранного отеля
    :param message: Полученное в чате сообщение
    :param bot: чат-бот
    :param p_type: тип изображения
    """
    msg = bot.send_message(message.chat.id, 'Сколько изображений вывести?\n'
                                            'Не более 10 изображений.')
    bot.register_next_step_handler(msg, show_picts, hot_id, bot, p_type)


def show_picts(message: types.Message, hot_id: str, bot, p_type: str) -> None:
    """
    Получает и выводит изображения отеля
    :param message: Полученное в чате сообщение
    :param hot_id: id отеля
    :param bot: чат-бот
    :param p_type: тип изображения
    """
    picts_count = message.text
    if not picts_count.isdigit() or int(picts_count) < 1 or int(picts_count) > 10:
        msg = bot.send_message(message.chat.id, 'Неправильный ввод. \nСколько изображений вывести?\n'
                                                'Не более 10 изображений.')
        bot.register_next_step_handler(msg, show_picts, hot_id, bot, p_type)
    else:
        logger.info('Получен id отеля для показа изображений')
        logger.info(hot_id)
        if p_type == 'h_pic':
            one_hotel = ApiSearch(config.X_RAPIDAPI_KEY, hot_id)
            logger.info('Отправлен запрос на получение изображений отеля')
            hotel_pics = one_hotel.get_hotel_pics()
            logger.info('Получен список url изображений отеля')
            logger.info(hotel_pics)
            for index, one_pict in enumerate(hotel_pics):
                if index < int(message.text):
                    final_pic = one_pict.replace('{size}', 'z')
                    bot.send_photo(chat_id=message.chat.id, photo=final_pic)
                    time.sleep(0.1)
                else:
                    break
        next_step: List = [('Показать последний запрос', str(message.chat.id) + '.his')]
        this_keyboard = BotKeyboard(next_step, 1)
        keyboard = this_keyboard.create_keys()
        bot.send_message(message.chat.id, text='Выберите действие: ', reply_markup=keyboard)


def show_history(message: types.Message) -> None:
    """
    Показывает результат последнего запроса пользователя
    :param message: Полученное в чате сообщение
    """
    hotels, tm_stamp = bot_database.select_some(message.chat.id, 'History', 'hotels', 't_stamp')
    hotels_list = json.loads(hotels)
    when = datetime.datetime.fromtimestamp(tm_stamp).strftime('%Y-%m-%d %H:%M')
    persons, city, hot_num, check_out, check_in, sorting_m, lang = bot_database.select_history(
        message.chat.id, tm_stamp, 'persons', 'city', 'hot_num',
        'check_out', 'check_in', 'state', 'lang')
    if sorting_m == 'low':
        sorting_m = 'lowprice'
    elif sorting_m == 'high':
        sorting_m = 'highprice'
    else:
        sorting_m = 'bestdeal'
    history_info = f'Последний запрос: \n' \
                   f'Время запроса: {when} \n' \
                   f'Команда: {sorting_m} \n' \
                   f'Город поиска: {city} \n' \
                   f'Дата заезда: {check_in} \n' \
                   f'Дата выезда: {check_out} \n' \
                   f'Количество проживающих: {persons} \n' \
                   f'Язык поиска: {lang} \n' \
                   f'Количество отелей: {hot_num} \n'
    bot.send_message(message.chat.id, history_info)
    inline_keyboard.show_hotels(message.chat.id, hotels_list, question='Результаты последнего запроса:')
